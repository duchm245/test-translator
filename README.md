// Chương trình eng-vie-trans thực hiện yêu cầu 1 :
Using 3 given csv files above to write a program (Java or Python script) to generate
English-Vietnamese translation csv file :

1. ID of English sentence (id)
2. Text of English sentence (text)
3. URL of English sentence’s audio (audio_url)
- format: https://audio.tatoeba.org/sentences/<Lang>/<Sentence id>.mp3
- ex: https://audio.tatoeba.org/sentences/eng/1319.mp3
4. ID of Vietnamese translation (translate_id)
5. Text of Vietnamese translation (translate_text)

// Chương trình test_translator thực hiện yêu cầu 2,3:
- Import translation csv file to PostgreSQL (https://www.elephantsql.com/) by another
program (Java or Python script)
- Make a public API service (Java Spring Boot) to show translation data with paging of
  10 records using database from above step:
<!--  -->

- [ ] step 1: chạy chương trình 1 eng-vie-trans
cần tải 3 file links.cvs, sentences.csv, sentences_with_audio.csv từ https://tatoeba.org/en/downloads rồi thêm vào thư mục resources
chạy chương trình để gen ra file output.csv


- [ ] step 2: chạy chương trình 2 test_translator
- copy file output.csv ở trên vào thư mục resources dự án
- tạo server postgreSql trên https://www.elephantsql.com/ rồi cấu hình kết nối trong file application.properties
- chạy chương trình và gọi api : http://localhost:8090/api/import để import data vào bảng TRANS_ENG_VIE trong database postgreSql
- gọi api http://localhost:8090/api/translations?page_number=2&page_size=10 để xem translation data


- [ ] step 3: sau khi deploy (tạm thời deploy lên railway, ...) thì có thể gọi api :
https://test-vinhome-production.up.railway.app/api/translations?page_number=2&page_size=10
