package org.example;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class TranslationEngVie {

    public static void main(String[] args) {
        String sentencesCsvPath = "src/main/resources/sentences.csv";
        String linksCsvPath = "src/main/resources/links.csv";
        String sentencesWithAudioCsvPath = "src/main/resources/sentences_with_audio.csv";
        String outputCsvPath = "src/main/resources/output.csv";

        try {
            generateTranslationCsv(sentencesCsvPath, linksCsvPath, sentencesWithAudioCsvPath, outputCsvPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void generateTranslationCsv(String sentencesCsvPath, String linksCsvPath, String sentencesWithAudioCsvPath, String outputCsvPath) throws IOException {
        try (BufferedReader sentencesReader = new BufferedReader(new FileReader(sentencesCsvPath));
             BufferedReader linksReader = new BufferedReader(new FileReader(linksCsvPath));
             BufferedReader sentencesWithAudioReader = new BufferedReader(new FileReader(sentencesWithAudioCsvPath));
             BufferedWriter writer = new BufferedWriter(new FileWriter(outputCsvPath))) {

            // Tạo Map eng và vie từ sentencesCsvPath
            Map<String, String> engMap = new HashMap<>();
            Map<String, String> vieMap = new HashMap<>();
            String sentenceLine;
            while ((sentenceLine = sentencesReader.readLine()) != null) {
                String[] sentenceData = sentenceLine.split("\t");
                if ("eng".equals(sentenceData[1])) {
                    engMap.put(sentenceData[0], sentenceData[2]);
                }
                if ("vie".equals(sentenceData[1])) {
                    vieMap.put(sentenceData[0], sentenceData[2]);
                }
            }

            // Tạo Map audioUrl từ sentencesWithAudioCsvPath
            Map<String, String> audioUrlMap = new HashMap<>();
            String audioLine;
            while ((audioLine = sentencesWithAudioReader.readLine()) != null) {
                String[] audioData = audioLine.split("\t");
                if (audioData.length >= 3 && !"\\N".equals(audioData[2])) {
                    String audioUrl = "https://audio.tatoeba.org/sentences/eng/" + audioData[0] + ".mp3";
                    audioUrlMap.put(audioData[0], audioUrl);
                }
            }

            // Đọc file links.csv rồi duyệt mảng linkData[engId, vieId]
            // nếu link[0] thuộc key của map eng và link[1] thuộc key của map vie thì ghi ra file output.csv
            String linkLine;
            while ((linkLine = linksReader.readLine()) != null) {
                String[] linkData = linkLine.split("\t");
                if (engMap.containsKey(linkData[0]) && vieMap.containsKey(linkData[1])) {
                    String audioUrl = audioUrlMap.getOrDefault(linkData[0], "");
                    writer.write(linkData[0] + "\t" + engMap.get(linkData[0]) + "\t" + audioUrl + "\t" + linkData[1] + "\t" + vieMap.get(linkData[1]));
                    writer.newLine();
                }
            }
        }
    }

}
